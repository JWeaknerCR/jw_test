########################################
# Setting up Resource Variables for Virtual Machine
########################################

variable "resource_group" {
  description = ""
  type        = "string"
}

variable "name" {
  description = "vm_size"
  type        = "string"
}

variable "delete_os_disk_on_termination" {
  description = ""
  type        = "string"
  default     = "true"
}

variable "delete_data_disks_on_termination" {
  description = ""
  type        = "string"
  default     = "true"
}

variable "private_ip_address_allocation" {
  description = ""
  type        = "string"
  default     = "Dynamic"
}

variable "subnet_vnet" {
  description = ""
  type        = "string"
}

variable "subnet_rg" {
  description = ""
  type        = "string"
}

variable "" {
  description = ""
  type        = "string"
}

