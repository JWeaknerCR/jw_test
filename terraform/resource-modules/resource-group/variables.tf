variable "region" {
  description = "Geographic region resource will be deployed into"
  type        = "string"
}
variable "name" {
  description = "Name of Resource Group"
  type        = "string"
}
