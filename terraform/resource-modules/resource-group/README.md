# **Resource Modules: Resource Group**

## Description

This TF module creates a resource group with the name derived from the input variables and then converted to upper case.

## Resources Created

- Resource Group

## Example Variables
```javascript
  resource_prefix = "vnet"
  region          = "westeurope"
  environment     = "p"
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| region | Geographic region resource will be deployed into | string | n/a | yes |
| tags | Map of tags that will be applied to the Resource Group | map | n/a | yes |
| name | Name of Resource Group | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| resource\_group\_name | Generated resource group name |

