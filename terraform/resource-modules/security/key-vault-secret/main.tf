#######################################
# Setting up Infra Modules for Key Vault
#######################################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"

}

data "azurerm_client_config" "current" {

}


#################
# Setting up Key Vault Secret
#################

