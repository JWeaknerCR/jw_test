#######################################
# Setting up Infra Modules for Key Vault
#######################################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"

}

data "azurerm_client_config" "current" {

}


#################
# Setting up Key Vault
#################

resource "azurerm_key_vault" "main" {
  name                = "${var.key_vault_name}"
  location            = "${data.azurerm_resource_group.rg.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"
  tenant_id           = "${data.azurerm_client_config.current.tenant_id}"

  sku {
    name = "${var.key_vault_sku_name}"
  }

  enabled_for_disk_encryption = "${var.key_vault_disk_encryption}"

}