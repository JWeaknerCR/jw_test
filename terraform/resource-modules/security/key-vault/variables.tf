########################################
## Setting up Key Vault Resource Variables
########################################
variable "resource_group" {
  description = "Resource Group of vNet"
  type        = "string"
}

variable "key_vault_name" {
  description = "Name of the key vault"
  type        = "string"
}

variable "key_vault_sku_name" {
  description = "The Name of the SKU used for this Key Vault. Possible values are standard and premium."
  type        = "string"
}

variable "key_vault_disk_encryption" {
  description = "Name of the Hub Vnet"
  type        = "string"
}
