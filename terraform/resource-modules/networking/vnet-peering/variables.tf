################################################
# Setting up Resource Variables for VNet Peering
################################################

variable "hub_resource_group" {
  description = ""
  type        = "string"
}

variable "hub_vnet_name" {
  description = ""
  type        = "string"
}

variable "spoke_resource_group" {
  description = ""
  type        = "string"
}

variable "spoke_vnet_name" {
  description = ""
  type        = "string"
}

variable "spoke_short_name" {
  description = ""
  type        = "string"
}
