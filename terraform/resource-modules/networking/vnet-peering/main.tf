###########################
# Setting up Data Resources
###########################

data "azurerm_resource_group" "hub_rg" {
  name = "${var.hub_resource_group}"
}

data "azurerm_virtual_network" "hub_vnet" {
  name                = "${var.hub_vnet_name}"
  resource_group_name = "${var.hub_resource_group}"
}

data "azurerm_resource_group" "spoke_rg" {
  name = "${var.spoke_resource_group}"
}

data "azurerm_virtual_network" "spoke_vnet" {
  name                = "${var.spoke_vnet_name}"
  resource_group_name = "${var.spoke_resource_group}"
}

#################################
# Setting up Hub to Spoke Peering
#################################

resource "azurerm_virtual_network_peering" "hub_to_spoke" {
  name                      = "hubto${var.spoke_short_name}"
  resource_group_name       = "${azurerm_resource_group.hub_rg.name}"
  virtual_network_name      = "${azurerm_virtual_network.hub_vnet.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.spoke_vnet.id}"
}

#################################
# Setting up Spoke to Hub Peering
#################################

resource "azurerm_virtual_network_peering" "spoke_to_hub" {
  name                      = "${var.spoke_short_name}tohub"
  resource_group_name       = "${azurerm_resource_group.spoke_rg.name}"
  virtual_network_name      = "${azurerm_virtual_network.spoke_vnet.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.hub_vnet.id}"
}




