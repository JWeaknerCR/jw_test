# **Resource Modules: VNet Peering**

## Description
This TF module can be used to create the virtual network peerings between a spoke network and the hub network in a hub and spoke networking model.

## Resources Created

- VNet Peering - Hub to Spoke
- VNet Peering - Spoke to Hub

