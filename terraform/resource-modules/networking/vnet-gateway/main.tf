
###########################
# Setting up VNET Gateway
###########################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"
}

data "azurerm_virtual_network" "vnet" {
  name                = "${var.vnet_name}"
  resource_group_name = "${var.resource_group}"
}

data "azurerm_public_ip" "pip" {
  name                = "${var.pip_name}"
  resource_group_name = "${var.resource_group}"
}

data "azurerm_subnet" "GatewaySubnet" {
  name                 = "GatewaySubnet"
  virtual_network_name = "${var.vnet_name}"
  resource_group_name  = "${var.resource_group}"
}

#################
# Setting up VNet Gateway
#################

resource "azurerm_virtual_network_gateway" "main" {
  name                = "${var.name}"
  location            = "${data.azurerm_resource_group.rg.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"

  type = "${var.type}"
  sku  = "${var.sku}"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = "${data.azurerm_public_ip.pip.id}"
    private_ip_address_allocation = "${var.ip_allocation}"
    subnet_id                     = "${data.azurerm_subnet.GatewaySubnet.id}"
  }

}
