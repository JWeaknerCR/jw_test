########################################
# Setting up Resource Variables for VNet
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}

variable "vnet_name" {
  description = "Name of VNET"
  type        = "string"
}

variable "pip_name" {
  description = "Name of the Public IP"
  type        = "string"
}

variable "name" {
  description = "The name of the Virtual Network Gateway. Changing the name forces a new resource to be created"
  type        = "string"
}

variable "type" {
  description = "The type of the Virtual Network Gateway. Valid options are Vpn or ExpressRoute. Changing the type forces a new resource to be created"
  type        = "string"
}

variable "sku" {
  description = "Configuration of the size and capacity of the virtual network gateway"
  type        = "string"
}

variable "ip_allocation" {
  description = "Defines how the private IP address of the gateways virtual interface is assigned. Valid options are Static or Dynamic"
  type        = "string"
  default     = "dynamic"
}

