########################################
# Setting up Resource Variables for VNet
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}
