
###########################
# Setting up VNET Gateway
###########################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"
}

data "azurerm_virtual_network" "vnet" {
  name                = "${var.vnet_name}"
  resource_group_name = "${var.resource_group}"
}

data "azurerm_virtual_network_gateway" "vnet_gw" {
  name                = "${var.pip_name}"
  resource_group_name = "${var.resource_group}"
}


#################
# Setting up VNet Gateway Connection
#################

resource "azurerm_virtual_network_gateway_connection" "vnet_gw_expressroute" {
  name                = "${var.name}"
  location            = "${data.azurerm_resource_group.rg.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"

  type                          = "ExpressRoute"
  virtual_network_gateway       = "${data.vnet_gw.us.id}"
  express_route_circuit_id      = "${data.vnet_gw.europe.id}"

  authorization_key = "4-v3ry-53cr37-1p53c-5h4r3d-k3y"
}


virtual_network_gateway_name
type
shared_key
auth_key