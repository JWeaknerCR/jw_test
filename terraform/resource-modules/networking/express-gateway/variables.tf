########################################
# Setting up Resource Variables for Express Route Circuit
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}

variable "name" {
  description = ""
  type        = "string"
}

variable "service_provider_name" {
  description = ""
  type        = "string"
}

variable "peering_location" {
  description = ""
  type        = "string"
}

variable "bandwidth_in_mbps" {
  description = ""
  type        = "integer"
  default     = 50
}

variable "sku_tier" {
  description = ""
  type        = "string"
}

variable "sku_family" {
  description = ""
  type        = "string"
}



resource_group
name
service_provider_name
peering_location
bandwidth_in_mbps
sku_tier
sku_family