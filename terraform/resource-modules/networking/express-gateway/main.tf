###########################
# Setting up Data Resources
###########################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"

}

#################
# Setting up Express Route Circuit
#################

resource "azurerm_express_route_circuit" "main" {
  name                  = "${var.name}"
  location              = "${data.azurerm_resource_group.vnet.location}"
  resource_group_name   = "${data.azurerm_resource_group.vnet.name}"
  service_provider_name = "${var.service_provider_name}"
  peering_location      = "${var.peering_location}"
  bandwidth_in_mbps     = "${var.bandwidth_in_mbps}"

  sku {
    tier   = "${var.sku_tier}"
    family = "${var.sku_family}"
  }

}


