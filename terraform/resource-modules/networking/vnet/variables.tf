########################################
# Setting up Resource Variables for VNet
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}

variable "vnet_name" {
  description = "The name of the vnet"
  type        = "string"
}

variable "vnet_address_ranges" {
  description = "This is a list of the ip address ranges for the vnet"
  type        = "list"
}

