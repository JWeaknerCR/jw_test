# **Resource Modules: Virtual Network**

## Description

This TF module can be used to create a basic virtual network.

## Resources Created

- Virtual Network

## Example Variables
```javascript
    resource_prefix = "vnet"
    region = "eastus"

    vnet_address_ranges = ["10.1.0.0/16"]

```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| resource\_group | The name of the target resource group | string | n/a | yes |
| vnet\_name | The name of the vnet | string | n/a | yes |
| vnet\_address\_ranges | This is a list of the ip address ranges for the vnet | list | n/a | yes |
| tier | The tier of a VNet, e.g HUB, or SPK | string | n/a | yes |
| tags | Map of tags that will be applied to the VNet | map | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| vnet\_rg\_name | Generated vnet resource group name |
| vnet\_rg\_id | Vnet owning resource group id |
| vnet\_name | Generated vnet name |
| vnet\_id | vnet resource id |

