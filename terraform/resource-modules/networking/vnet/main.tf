###########################
# Setting up Resource Group
###########################

data "azurerm_resource_group" "vnet" {
  name = "${var.resource_group}"
}

#################
# Setting up VNet
#################

resource "azurerm_virtual_network" "main" {
  name                = "${var.vnet_name}"
  address_space       = "${var.vnet_address_ranges}"
  location            = "${data.azurerm_resource_group.vnet.location}"
  resource_group_name = "${data.azurerm_resource_group.vnet.name}"


}
