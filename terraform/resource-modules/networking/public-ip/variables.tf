########################################
# Setting up Resource Variables for PIP
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}

variable "pip_name" {
  description = "Specifies the name of the Public IP resource"
  type        = "string"
}

variable "allocation_method" {
  description = "Defines the allocation method for this IP address. Possible values are Static or Dynamic"
  type        = "string"
  default     = "Static"
}

