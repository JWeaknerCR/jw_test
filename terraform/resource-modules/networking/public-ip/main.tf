###########################
# Setting up Resource Group
###########################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"

}

#################
# Setting up Public IP
#################
resource "azurerm_public_ip" "main" {
  name                = "${var.pip_name}"
  location            = "${data.azurerm_resource_group.rg.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"
  allocation_method   = "${var.allocation_method}"

}