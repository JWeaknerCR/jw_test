###########################
# Setting up Resource Group
###########################

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"

}

data "azurerm_virtual_network" "vnet" {
  name = "${var.vnet_name}"
}

data "azurerm_public_ip" "pip" {
  name = "${var.pip_name}"
}

data "azurerm_subnet" "gw_subnet" {
  name                 = "${var.resource_group}"
  virtual_network_name = "${var.vnet_name}"
  resource_group_name  = "${var.resource_group}"
}

#################
# Setting up Firewall
#################
resource "azurerm_firewall" "main" {
  name                = "${var.firewall_name}"
  location            = "${data.azurerm_resource_group.rg.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"

  ip_configuration {
    name                 = "configuration"
    subnet_id            = "${data.azurerm_subnet.gw_subnet.id}"
    public_ip_address_id = "${data.azurerm_public_ip.pip.id}"
  }
}