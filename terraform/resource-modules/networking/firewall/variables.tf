########################################
# Setting up Resource Variables for PIP
########################################

variable "resource_group" {
  description = "The name of the target resource group"
  type        = "string"
}

variable "firewall_name" {
  description = "Specifies the name of the Firewall resource"
  type        = "string"
}



