output "pip_id" {
  value       = "${azurerm_public_ip.main.ip_address}"
  description = "The IP address value that was allocated"
}

