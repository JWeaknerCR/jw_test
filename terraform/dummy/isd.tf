_azu_hub_base_output_

provider "azurerm" {
    version = "=1.36.0"
}
 
data "azurerm_client_config" "current" {}
 
 
resource "azurerm_resource_group" "AZU-BSL-SSVS-EXPRESSROUTE-RG" {
    name     = "AZU-BSL-SSVS-EXPRESSROUTE-RG"
    location = "UK South"
}
 
resource "azurerm_express_route_circuit" "AZU-BSL-SVSS-EXPRESSROUTE-LONDON" {
    name                  = "AZU-BSL-SVSS-EXPRESSROUTE-LONDON"
    resource_group_name   = "${azurerm_resource_group.AZU-BSL-SSVS-EXPRESSROUTE-RG.name}"
    location              = "${azurerm_resource_group.AZU-BSL-SSVS-EXPRESSROUTE-RG.location}"
    service_provider_name = "Megaport"
    peering_location      = "London"
    bandwidth_in_mbps     = "50"
 
    sku {
        tier = "Standard"
        family = "MeteredData"
}
 
}
  
_ISD_

provider "azurerm" {
    version = "=1.36.0"
}
 
data "azurerm_client_config" "current" {}
 
 
 
resource "azurerm_resource_group" "AZU-ISD-FTSE-SID-NP-RG" {
    name     = "AZU-ISD-FTSE-SID-NP-RG"
    location = "UK South"
}
 
 
resource "azurerm_virtual_network" "AZU-ISD-FTSE-SID-NP-VNET" {
    name                = "AZU-ISD-FTSE-SID-NP-VNET"
    address_space       = ["10.138.68.0/23"]
    location            = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.location}"
    resource_group_name = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.name}"
 
}
 
 
resource "azurerm_subnet" "Gateway" {
    name                 = "Gateway"
    resource_group_name  = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.name}"
    virtual_network_name = "${azurerm_virtual_network.AZU-ISD-FTSE-SID-NP-VNET.name}"
    address_prefix       = "10.138.68.0/26"
 
}
resource "azurerm_subnet" "ASE" {
    name                 = "ASE"
    resource_group_name  = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.name}"
    virtual_network_name = "${azurerm_virtual_network.AZU-ISD-FTSE-SID-NP-VNET.name}"
    address_prefix       = "10.138.69.0/24"
}
resource "azurerm_subnet" "DB" {
    name                 = "DB"
    resource_group_name  = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.name}"
    virtual_network_name = "${azurerm_virtual_network.AZU-ISD-FTSE-SID-NP-VNET.name}"
    address_prefix       = "10.138.68.64/26"
}
resource "azurerm_subnet" "API" {
    name                 = "API"
    resource_group_name  = "${azurerm_resource_group.AZU-ISD-FTSE-SID-NP-RG.name}"
    virtual_network_name = "${azurerm_virtual_network.AZU-ISD-FTSE-SID-NP-VNET.name}"
    address_prefix       = "10.138.68.128/26"
}
 
