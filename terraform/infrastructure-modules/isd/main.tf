#######################################
# Setting up Infra Modules for Hub vNet
#######################################

module "resource_group" {
  source = "../../resource-modules/resource-group"
  region = var.region
  name   = var.rg_name
}

module "vnet_hub" {
  source              = "../../resource-modules/networking/vnet"
  resource_group      = module.resource_group.resource_group_name
  vnet_name           = var.vnet_name
  vnet_address_ranges = var.vnet_address_ranges

}

module "vnet-subnets-spoke" {
  source         = "../../resource-modules/networking/vnet-spoke"
  resource_group = module.resource_group.resource_group_name
  vnet_name      = module.vnet_hub.vnet_name
  subnets        = var.subnets_spoke
}

module "hub-to-spoke-peering" {
  source         = "../../resource-modules/networking/vnet-peering"
  hub_resource_group    = ""
  hub_vnet_name         = ""
  spoke_resource_group  = module.resource_group.resource_group_name
  spoke_vnet_name       = module.vnet_hub.vnet_name
  spoke_short_name      = "ISD"
}



