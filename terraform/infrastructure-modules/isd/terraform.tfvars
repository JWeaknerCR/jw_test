#####
# Resource Group
#####

rg_name = "AZU-ISD-FTSE-SID-NP-RG"
region  = "UK South"

#####
# VNET Name 
#####

vnet_name          = "AZU-ISD-FTSE-SID-NP-VNET"
vnet_address_ranges = "10.138.68.0/23"

#####
# Subnet Spoke
#####

subnets_spoke = [
  {
    name        = "Gateway"
    subnet_cidr = "10.138.68.0/26"
  },
  {
    name        = "ASE"
    subnet_cidr = "10.138.69.0/24"
  },
  {
    name        = "DB"
    subnet_cidr = "10.138.68.64/26"
  },
  {
    name        = "API"
    subnet_cidr = "10.138.68.128/26"
  }
]
