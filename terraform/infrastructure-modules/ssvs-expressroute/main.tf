#######################################
# Setting up Infra Modules for Hub vNet
#######################################

module "resource_group" {
  source = "../../resource-modules/resource-group"
  region = var.region
  name   = var.rg_name
}




module "key_vault" {
  source                    = "../../resource-modules/security/key-vault"
  resource_group            = module.resource_group.resource_group_name
  key_vault_name            = var.key_vault_name
  key_vault_sku_name        = var.key_vault_sku_name
  key_vault_disk_encryption = var.key_vault_disk_encryption
}