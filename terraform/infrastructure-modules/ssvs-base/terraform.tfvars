#####
# Resource Group
#####

rg_name = "AZU-BSL-SVSS-Base-NP-RG"
region  = "UK South"

#####
# Key vault vars
#####

key_vault_name            = "myjordankeyvault"
key_vault_sku_name        = "standard"
key_vault_disk_encryption = "true"