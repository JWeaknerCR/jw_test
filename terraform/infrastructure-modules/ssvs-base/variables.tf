variable "rg_name" {
  description = "Name of the Resource Group"
  type        = "string"
}

variable "region" {
  description = "Geographic Region resource will be deployed into"
  type        = "string"
}

variable "key_vault_name" {
  description = "Name of the key vault"
  type        = "string"
}

variable "key_vault_sku_name" {
  description = "The Name of the SKU used for this Key Vault. Possible values are standard and premium."
  type        = "string"
}

variable "key_vault_disk_encryption" {
  description = "Name of the Hub Vnet"
  type        = "string"
}
