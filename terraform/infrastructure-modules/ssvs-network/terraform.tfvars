#####
# Resource Group
#####

rg_name = "AZU-BSL-SVSS-Gateway-NP-RG"
region  = "Uk South"

#####
# VNET Spoke
#####

vnet_name           = "myvnet"
vnet_address_ranges = ["10.138.64.0/22"]

#####
# Subnet Spoke
#####

subnets_spoke = [
  {
    name        = "AzureFirewallSubnet"
    subnet_cidr = "10.138.64.64/26"
  },
  {
    name        = "SSVS"
    subnet_cidr = "10.138.65.0/24"
  },
  {
    name        = "ImageBuild"
    subnet_cidr = "10.138.66.0/26"
  },
  {
    name        = "GatewaySubnet"
    subnet_cidr = "10.138.64.0/26"
  }
]

#####
# Public IP
#####

pip_name = "mypip"

#####
# VNET GATEWAY
#####

vnet_gw_name          = "AZU-BSL-SSVS-NP-GW-01"
vnet_gw_type          = "ExpressRoute"
vnet_gw_sku           = "Standard"
vnet_gw_ip_allocation = "Dynamic"
