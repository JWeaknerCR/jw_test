#######################################
# Setting up Infra Modules for Hub vNet
#######################################

module "resource_group" {
  source = "../../resource-modules/resource-group"
  region = var.region
  name   = var.rg_name
}

module "vnet_hub" {
  source              = "../../resource-modules/networking/vnet"
  resource_group      = module.resource_group.resource_group_name
  vnet_name           = var.vnet_name
  vnet_address_ranges = var.vnet_address_ranges
}

module "vnet-subnets-spoke" {
  source         = "../../resource-modules/networking/vnet-spoke"
  resource_group = module.resource_group.resource_group_name
  vnet_name      = module.vnet_hub.vnet_name
  subnets        = var.subnets_spoke
}

module "vnet_gateway" {
  source         = "../../resource-modules/networking/vnet-gateway"
  resource_group = module.resource_group.resource_group_name
  vnet_name      = module.vnet_hub.vnet_name
  pip_name       = var.pip_name
  name           = var.vnet_gw_name
  type           = var.vnet_gw_type
  sku            = var.vnet_gw_sku
  ip_allocation  = var.vnet_gw_ip_allocation
}

module "public_ip" {
  source         = "../../resource-modules/networking/public-ip"
  resource_group = module.resource_group.resource_group_name
  pip_name       = var.pip_name
}

