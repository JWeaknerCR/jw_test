output "vnet_hub_name" {
  value       = "${module.vnet_hub.vnet_name}"
  description = "Generated hub vnet name"
}

output "vnet_hub_id" {
  value       = "${module.vnet_hub.vnet_id}"
  description = "Resource id of hub vnet"
}

