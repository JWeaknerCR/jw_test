########################################
## Setting up Network Resource Variables
########################################
variable "rg_name" {
  description = "Name of the Resource Group"
  type        = "string"

}

variable "region" {
  description = "Geographic Region resource will be deployed into"
  type        = "string"
}

variable "vnet_name" {
  description = "Name of the Hub Vnet"
  type        = "string"

}

variable "vnet_address_ranges" {
  description = "This is a list of the ip address ranges for the vnet"
  type        = "list"
}

variable "tier" {
  description = "The tier of a VNet, e.g HUB, or SPK"
  type        = "string"
  default     = "hub"
}

variable "subnets_spoke" {
  description = "Map of subnets with name, subnet_cidr"
  type        = "list"
}

variable "pip_name" {
  description = "Specifies the name of the Public IP resource"
  type        = "string"
}

variable "vnet_gw_name" {
  description = "The name of the Virtual Network Gateway. Changing the name forces a new resource to be created."
  type        = "string"
}

variable "vnet_gw_type" {
  description = "The type of the Virtual Network Gateway. Valid options are Vpn or ExpressRoute. Changing the type forces a new resource to be created"
  type        = "string"
}

variable "vnet_gw_sku" {
  description = "Configuration of the size and capacity of the virtual network gateway"
  type        = "string"
}

variable "vnet_gw_ip_allocation" {
  description = "Defines how the private IP address of the gateways virtual interface is assigned. Valid options are Static or Dynamic"
  type        = "string"
  default     = "dynamic"
}



